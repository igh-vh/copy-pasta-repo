# Collection of C++ Headers

## wrapper_base.h

This header helps you to convert a C Library and its handles
into a C++ Class, which is safe against copying and moving and slicing.
The class is comparable to `std::unique_ptr`.

Lets say, you have a C library with the following methods:
```c++
struct mycontext;
struct mycontext * mylib_init();
void mylib_free(struct mycontext *);
int mylib_work(struct mycontext*, const char *arg);
```
The first one hands you a pointer to an (obscure) context struct
(or `NULL` on error), the second one can free it.
The third function does some work with the context
and returns 0 on success.

With this Wrapper Base class, you can wrap for a more C++-ish coding style:

```c++
class MyLib : public WrapperBase<mycontext, mylib_free>
{
    MyLib() : WrapperBase(mylib_init(), WrapperBase::nullcheck) {}
    void work(const char *arg)
    {
        if (mylib_work(get(), arg) != 0)
            throw std::runtime_error("work() failed");
    }
};

void test()
{
    Mylib lib;
    lib.work("hello there");
}
```
Now, exceptions are thrown if an error occurs
and the lifetime of the library handle is clearly defined.

/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef EXPAT_WRAPPER_IMPL_H
#define EXPAT_WRAPPER_IMPL_H

#include "expat_wrapper.h"

#include <expat.h>
#include <expat_external.h>
#include <stdexcept>


inline void XmlParserDeleter::operator()(XML_ParserStruct* s) noexcept
{
    if (s)
        XML_ParserFree(s);

}

template<typename Handler>
ExpatWrapper<Handler>::ExpatWrapper(const char *encoding)
    : parser_(XML_ParserCreate(encoding))
{
    if (!parser_)
        throw std::runtime_error("Could not create XML parser");

    XML_SetUserData(parser_.get(), this);

    XML_SetStartElementHandler(parser_.get(), static_cast<XML_StartElementHandler>(
        [](void *userdata, const char *name, const char** atts){
            auto& This = *reinterpret_cast<ExpatWrapper<Handler>*>(userdata);
            try {
                static_cast<Handler&>(This).startElement(name, atts);
            } catch(...) {
                This.pending_exception_ = std::current_exception();
                XML_StopParser(This.parser_.get(), false);
            }
        }
    ));

    XML_SetEndElementHandler(parser_.get(), static_cast<XML_EndElementHandler>(
        [](void *userdata, const char *name)
        {
            auto& This = *reinterpret_cast<ExpatWrapper<Handler>*>(userdata);
            if (This.pending_exception_)
                return;
            try {
                // Handler derives from us, so we can downcast ourself.
                static_cast<Handler&>(This).endElement(name);
            } catch(...) {
                This.pending_exception_ = std::current_exception();
                XML_StopParser(This.parser_.get(), false);
            }
        }
    ));

    XML_SetCharacterDataHandler(parser_.get(), static_cast<XML_CharacterDataHandler>(
        [](void *userdata, const char *s, int len)
        {
             auto& This = *reinterpret_cast<ExpatWrapper<Handler>*>(userdata);
            if (This.pending_exception_)
                return;
            try {
                 static_cast<Handler&>(This).characterData(s, len);
            } catch(...) {
                This.pending_exception_ = std::current_exception();
                XML_StopParser(This.parser_.get(), false);
            }
        }
    ));
}

template<typename Handler>
inline bool ExpatWrapper<Handler>::parse(const char *s, std::size_t n, bool final)
{
    if (XML_STATUS_OK == XML_Parse(parser_.get(), s, n, final))
        return true;

    // rethrow exception
    const XML_Error err_code = XML_GetErrorCode(parser_.get());
    if (err_code == XML_ERROR_ABORTED && pending_exception_)
        std::rethrow_exception(std::exchange(pending_exception_, nullptr));

    static_cast<Handler*>(this)->xmlError(XML_ErrorString(err_code));
    return false;
}

#endif  // EXPAT_WRAPPER_H

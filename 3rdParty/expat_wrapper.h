/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef EXPAT_WRAPPER_H
#define EXPAT_WRAPPER_H

#include <exception>
#include <functional>
#include <memory>

extern "C"
{
/// forward declaration of exapt internals.
struct XML_ParserStruct;
}

/// custom deleter for unique_ptr to release the exapt handle.
struct XmlParserDeleter
{
    void operator()(XML_ParserStruct*) noexcept;
};

/**
 * \brief Expat XML parser wrapper
 *
 * This is a C++ wrapper around the expat library,
 * using the CRTP pattern. The user is expected to
 * make a derived class from this class and pass the
 * name of the derived class as template argument.
 * The derived class is expected to have four member functions:
 * \code
 * class Derived : protected ExpatWrapper<Derived>
 * {
 * public:
 *     void startElement(const char *name, const char ** atts);
 *     void endElement(const char *name);
 *     void characterData(const char *s, int len);
 *     void xmlError(const char *err_message);
 * };
 * \endcode
 * These member functions have eighter to be public,
 * else this base class has to be declared as a friend.
 * All callback functions are allowed to throw exceptions.
 * If an error occurs during parse(), xmlError() is called.
 */
template<typename Handler>
class ExpatWrapper
{
public:
    /** \brief Create a new parser instance.
     * \throw std::runtime_error Parser could not be created.
     */
    ExpatWrapper(const char *encoding = "UTF-8");
    ExpatWrapper(ExpatWrapper&&) = default;
    ExpatWrapper& operator=(ExpatWrapper&&) = default;

protected:
    /** \brief Provide new input for the parser.
     *
     * Basically calls XML_Parse() and returns whether it was successful.
     * Exceptions from the callbacks are forwarded through the library
     * and rethrown in this method.
     * \param [in] s Input data.
     * \param [in] n Length of input data, in bytes.
     * \param [in] final Whether this is the final call of parse(), default false.
     * \return true on success, else false.
     */
    bool parse(const char *s, std::size_t n, bool final = false);
    ~ExpatWrapper() = default;

private:
    /// \brief XML parser handle from the library.
    std::unique_ptr<XML_ParserStruct, XmlParserDeleter> parser_;
    /// \brief Exception pointer to tunnel exceptions through expat.
    std::exception_ptr pending_exception_ = nullptr;
};

#endif  // EXPAT_WRAPPER_H

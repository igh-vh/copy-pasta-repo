/*****************************************************************************
 *
 *  Copyright 2021 Bjarne von Horn
 *
 *  This file is part of the copy pasta library.
 *
 *  The copy pasta library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The copy pasta library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the copy pasta library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include <utility>
#include <stdexcept>

/** Base class for RAII wrapper
 *
 * This class manages raw pointers (e.g. from C libraries)
 * and makes sure that the resource is only free'd once.
 *
 * Let's say you have a C library with the following methods:
 * \code
 * struct mycontext;
 * struct mycontext * mylib_init();
 * void mylib_free(struct mycontext *);
 * int mylib_work(struct mycontext*, const char *arg);
 * \endcode
 * The first one hands you a pointer to an (obscure) context struct
 * (or \c NULL on error), the second one can free it.
 * The third function does some work with the context
 * and returns 0 on success.
 *
 * With this Base class, you can wrap for a more C++-ish coding style:
 *
 * \code
 * class MyLib : public WrapperBase<mycontext, mylib_free>
 * {
 *     MyLib() : WrapperBase(mylib_init(), WrapperBase::nullcheck) {}
 *     void work(const char *arg)
 *     {
 *          if (mylib_work(get(), arg) != 0)
 *              throw std::runtime_error("work() failed");
 *     }
 * };
 * void test()
 * {
 *     Mylib lib;
 *     lib.work("hello there");
 * }
 * \endcode
 * When you use this \c MyLib instance, the whole error checking is done for you.
 * So, if no context could be created or \c mylib_work() failed,
 * an exception is thrown.
 *
 * \tparam T Type of the resource, may be incomplete.
 * \tparam deleter Function which takes a \c T* and free's it up.
 */
template <typename T, void (*deleter)(T *)>
class WrapperBase
{
    /// The resource handle.
    T *object_;

public:
    struct nullcheck_t
    {
    };
    /// Tag for enabling nullptr-check in constructor.
    static constexpr nullcheck_t nullcheck{};

    /** Constructor without \c nullptr check.
     *
     * \param obj Preallocated resource handle, may be NULL.
     */
    explicit WrapperBase(T *obj) noexcept : object_(obj) {}
    /** Constructor with \c nullptr check.
     *
     * To select this overload, you have to pass
     * <tt>WrapperBase::nullcheck</tt>
     * as second argument.
     *
     * \param obj Preallocated resource handle, may be NULL.
     * \throws std::runtime_error if the preallocated resource handle is NULL.
     */
    WrapperBase(T *obj, nullcheck_t) : object_(obj)
    {
        if (!object_)
            throw std::runtime_error("could not create instance");
    }

    /// Deleted Copy constructor.
    WrapperBase(WrapperBase const &) = delete;
    /** Move constructor.
     * \param other Wrapper to steal the handle from.
     */
    WrapperBase(WrapperBase &&other) noexcept : object_(nullptr)
    {
        other.swap(*this);
    }
    /// Deleted copy assignment operator.
    WrapperBase &operator=(WrapperBase const &) = delete;
    /** Move assignment operator.
     *
     * Safe against self-assignment due to the copy-and-swap idiom.
     *
     * \param other Wrapper to steal the handle from.
     */
    WrapperBase &operator=(WrapperBase &&other) noexcept
    {
        WrapperBase copy(std::move(other));
        copy.swap(*this);
        return *this;
    }

    /** Specialization for std::swap
     *
     * Directly swap the contents of two instances.
     *
     * \param other Wrapper to exchange the handle.
     */
    void swap(WrapperBase &other) noexcept {
        std::swap(object_, other.object_)}

    /** Borrow the handle.
     *
     * Do not free the handle.
     *
     * \return the handle.
     */
    T *get() const noexcept
    {
        return object_;
    }

    /** Steal the handle
     *
     * The wrapper no longer takes care of the destruction
     * of the handle, you have to free it by yourself.
     *
     * \return the handle.
     */
    T *release() noexcept
    {
        return std::exchange(object_, nullptr);
    }

    /** Free the handle.
     *
     * The handle is free'd by the \c deleter function
     * and then set to NULL.
     */
    void reset() noexcept
    {
        if (object_)
            deleter(object_);
    }

protected:
    ~WrapperBase()
    {
        reset();
    }
};
